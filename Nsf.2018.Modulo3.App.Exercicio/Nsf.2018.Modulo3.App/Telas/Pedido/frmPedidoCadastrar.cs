﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Pedido;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoCadastrar : UserControl
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
    {
        public frmPedidoCadastrar()
        {
            InitializeComponent();
            CarregarCombos();   
        }   ConfigurarGrid();

    void CarregarCombos()
    {
        ProdutoBusiness business = new ProdutoBusiness();
        List<ProdutoDTO> lista = business.Listar();
        cboProduto.ValueMember = nameof(ProdutoDTO.id);
        cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
        cboProduto.DataSource = lista;
    }
    void ConfigurarGrid()
    {
        dgvItens.AutoGenerateColums = false;
        dgvItens.DataSource = produtosCarrinho;
    } 

    private void btnEmitir_Click(object sender, EventArgs e)
    {
        PedidoDTO dto = new PedidoDTO();
        dto.Cliente = txtCliente.Text;
        dto.Cpf = txtCpf.Text;
        dto.Data = DateTime.Now;
        PedidoBusiness business = new PedidoBusiness();
        business.Salvar(dto, produtosCarrinho.ToList());
        MessageBox.Show("Pedido Salvo Com Sucesso.", "DBZ", MessageBoxButtons.OK, MessageBoxIcon.Information);
        this.Hide();
    }
    private void btnAdd_Click(object sender, EventArgs e)
    {
        ProdutoDTO dto = cboProduto.SelecItem as ProdutoDTO;
        for (int i = 0; i < int.Parse(TxtQuantidade,Text); i++)
        {
            produtosCarrinho.Add(dto);
        }
        
    }

    
    }
}
